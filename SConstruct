# gnu arm toolchain must be already in system path

import os

# -----------------------------------------------------------------------------------
Help("""
Type: 'scons' to build the source files in the src/ directory,
      'scons -c' to clean (combined with the above flags)
""")


# -----------------------------------------------------------------------------------
# Project name - this is the name of the output
# (.elf/.hex) file.
#
PROJ_NAME = 'Application'

# -----------------------------------------------------------------------------------
# Output (variant) directory
#
VDIR = 'build/debug/'

# -----------------------------------------------------------------------------------
# The cross compiler must be in your $PATH
#
env = Environment(ENV = os.environ)

env.Replace(AS="arm-none-eabi-as")
env.Replace(AR="arm-none-eabi-ar")
env.Replace(CC="arm-none-eabi-gcc")
env.Replace(CXX="arm-none-eabi-g++")
env.Replace(LINK="arm-none-eabi-g++")
env.Replace(OBJCOPY="arm-none-eabi-objcopy")
env.Replace(PROGSUFFIX=".elf")
env.Replace(RANLIB="arm-none-eabi-ranlib")
env.Replace(SIZE="arm-none-eabi-size")

# -----------------------------------------------------------------------------------
# Processor-specific compiler flags
#
CORTEX_M4_FLAGS = [
    '-mcpu=cortex-m4',
    '-mthumb',
    '-mfloat-abi=soft',
    ]


# -----------------------------------------------------------------------------------
# C language-specific compiler flags
#
env.Append(CFLAGS=[
    '-std=gnu99',
])

# -----------------------------------------------------------------------------------
# C++ language-specific compiler flags
#
env.Append(CXXFLAGS=[
    '-std=gnu++14',
    '-fabi-version=0',
    '-fno-use-cxa-atexit',
    '-fno-threadsafe-statics',
])

# -----------------------------------------------------------------------------------
# General compilation flags
#
env.Append(CCFLAGS=[
    '-Og',
    '-fmessage-length=0',
    '-fsigned-char',
    '-ffunction-sections',
    '-fdata-sections',
    # '-ffreestanding',
    '-fno-move-loop-invariants',
    '-Wall',
    '-Wextra',
    '-g3',
    '-MMD',
    '-MP'] + CORTEX_M4_FLAGS)


# -----------------------------------------------------------------------------------
# Linker flags
#
env.Append(LINKFLAGS=CORTEX_M4_FLAGS + [
    '-Tldscripts/mem.ld',
    '-Tldscripts/sections.ld',
    '-nostartfiles',
    '-Xlinker',
    '--gc-sections',
    '-u', '_printf_float',
    '-u', '_scanf_float',
    '-Wl,-Map,"%s.map"'%(VDIR  + PROJ_NAME),   # put map file in build directory
    ])


# -----------------------------------------------------------------------------------
# Construct the source code directories and include paths for
# all sub-directories
# 
sources = []
includes = []

# -----------------------------------------------------------------------------------
# Hard code the application source files.  This means we don't
# have an SConscript file in the /src folder.  That way, the
# student can't (accidentally) delete it.
#
sources += Glob('#src/*.c')

env['CPPPATH']=includes


# -----------------------------------------------------------------------------------
# Build everything
#
TARGET_NAME = VDIR  + PROJ_NAME  + '.elf'

PRG = env.Program(
    target=TARGET_NAME,
    source=sources,
)


# -----------------------------------------------------------------------------------
# Hex file builder
# Invokes: Cross ARM GNU Create Flash Image
#
def hex_generator(source, target, env, for_signature):
    return '$OBJCOPY -O ihex %s %s'%(source[0], target[0])

env.Append(BUILDERS={
    'Obj_copy': Builder(
        generator=hex_generator,
        suffix='.hex',
        src_suffix='.elf'
    )
})

# -----------------------------------------------------------------------------------
# File size builder
# Invokes: Cross ARM GNU Print Size
#
def size_generator(source, target, env, for_signature):
    return '$SIZE --format=berkley -x %s'%(source[0])
#    return '$SIZE --format=sysv -x %s'%(source[0])

env.Append(BUILDERS={
    'Size': Builder(
        generator=size_generator,
        src_suffix='.elf'
    )
})

env.Obj_copy(PRG)
env.Size(PRG)
