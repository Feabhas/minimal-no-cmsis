// C11 freestanding minimum requirement
#include <float.h>
#include <iso646.h>
#include <limits.h>
#include <stdalign.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdnoreturn.h>


enum LED { D6 = 8, D5, D4, D3 };

static void sleep(void);

// Set up the register pointers
//
static volatile unsigned *const portd_moder = (unsigned *)0x40020C00;
static volatile unsigned *const portd_odr   = (unsigned *)0x40020C14;
static volatile unsigned *const rcc_ahb1enr = (unsigned *)0x40023830;

#pragma GCC diagnostic ignored "-Wmain"
void main(void)
{
    *rcc_ahb1enr |= (1u << 3); // enable PortD's clock

    unsigned  moder = *portd_moder;
    moder |= ((1u << D3*2)|(1u << D4*2)|(1u << D5*2)|(1u << D6*2));
    moder &= ~( (1u << ((D3 * 2)+1)) | (1u << ((D4 * 2)+1)) | (1u << ((D5 * 2)+1)) | (1u << ((D6 * 2)+1)));
    *portd_moder = moder;

loop_forever:
    *portd_odr |= (1u << D6);    // led-on
    sleep();
    *portd_odr &= ~(1u << D6);   // led-off
    sleep();
    goto loop_forever;
} 

void sleep(void) {
  volatile unsigned x = 500000; // arbitary value long enough to see
  while (x-- > 0) {
    __asm volatile("nop");
  }
}
