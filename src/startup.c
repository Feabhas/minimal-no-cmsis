
typedef void (*const pHandler)(void);

extern unsigned int __stack;           // defined in linker script

void Reset_Handler(void);
// void NMI_Handler(void);
void HardFault_Handler(void);

// Data marked with __attribute__((used)) is tagged in the object file to avoid
// removal by linker unused section removal.
__attribute__((section(".isr_vector"), used)) pHandler __isr_vectors[] = {
    // Cortex-M Core Handlers
    (pHandler)&__stack,                //  initial stack pointer
    Reset_Handler,                     //  Reset handler

    (pHandler)0,                        // Non-Maskable Interrupt handler
    HardFault_Handler,                  // Hard Fault handler
};

// stop the compiler compalining aboit void main(void)
#pragma GCC diagnostic ignored "-Wmain" 
void main(void);

void __attribute__((noreturn))
Reset_Handler(void)
{
    main();
    while (1)
      ;
}

// noreturn help compiler optimisation 
// void __attribute__((noreturn))
// NMI_Handler(void)
// {
//     while (1)
//         ;
// }

void __attribute__((noreturn))
HardFault_Handler(void) {
  while (1);
}
