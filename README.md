# Summary

Possibly the smallest complete ARMv7-M implementation for flashing the LED.

There are three files:

* startup.c - this contains all the C code including the IVT and the minimal set of exception handlers 
  required by a Cortex-M system
* main.c - sample led on/off program
* mem.ld - memory addresses
* sectons.ld - small linker script for code, rodata, bss, data and stack
* build.sh - script to build using CMake
* CMakelists.txt and toolchain-STM32F407.cmake - CMake config files

## To build

```
$ ./build.sh
```

Via Docker

```
$ docker run --rm -v $(pwd):/usr/project feabhas/gcc11-arm-cmake-alpine-ms
```

## View disassembler

```
$ arm-none-eabi-objdump -d build/debug/Application.elf
```

or too see all sections
```
$ arm-none-eabi-objdump -D build/debug/Application.elf
```

### Assembler

The only bit of assembler is:
```c
    __asm volatile("nop");
```

## Caveat

There is currently no error checking, it does not support statics, heap based or C++ (constructors, vtables, etc.).

## Running the code

In a terminal"
```
$ cd scripts
$ source ./download.sh
```

This will download and run the application using `JLinkExe`

## Testing the code

In one terminal:
```
$ JLinkGDBServerCL -USB -device STM32F407VG
```
This will start up the GDB Command Line Server for JLink over USB. This can be left running until you've completely finished, you don't need to keep restarting it for each debug session.

In another terminal window:
```
$ arm-none-eabi-gdb -x gdb_init -se build/debug/Application.elf
```
This will invoke the command line gdb client to connect to JLinkGDBServerCL.

It downloads the elf file, sets a breakpoint at main, runs to that point and then lists the C code.

The prompt will be:
```
(arm-gdb)
```

To run (continue) the program:
```
(arm-gdb) c
```
While running, to stop type `^C`.

To exit type:
```
(arm-gdb) q
A debugging session is active.

	Inferior 1 [Remote target] will be killed.

Quit anyway? (y or n) y
$
```
You can now reload a newly build elf file by repeating:
```
$ arm-none-eabi-gdb -x gdb_init -se build/debug/Application.elf
```
The gdb flags are:
* `-x` script file to run
* `-e` executable file
* `-s` symbol file

# Meson Build

To build with meson, first (only needs doing once)
```
$ meson builddir/ --cross-file cross.build && cd buildir
```
where `builddir` is the name of the build directory

To build:
```
$ ninja
```
Ninja will automatically detect any changes to the meson.build file

This will create a `Application.elf` file in the `builddir/src` directory. Use the `gdb_init` file to run; from the build dir:
```
$ arm-none-eabi-gdb -x ../gdb_init -se src/Application.elf
```
